import React, { Component } from 'react';

class NotFoundPage extends Component {
    render() {
        return (
            <div className="container">
                <h1>NotFoundPage</h1>

                <div class="alert alert-warning">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <strong>Not Found Page !</strong> Không tìm thấy trang
                </div>

            </div>



        );
    }

}

export default NotFoundPage;
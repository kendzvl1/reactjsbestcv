import React, { Component } from 'react';
import callApi from './../../utils/apiCaller';
import swal from 'sweetalert';
class ProductActionPage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: '',
            txtName: '',
            txtPrice: '',
            txtMoney: ''
        };
    }

    onChange = (e) => {
        var target = e.target;
        var name = target.name;
        var value = target.value;
        this.setState({
            [name]: value
        });
    }
    onSave = (e) => {
        e.preventDefault();
        var { id, txtName, txtPrice, txtMoney } = this.state;
        if (id) {
            callApi('updateProduct', 'PATCH', {
                id: id,
                name: txtName,
                price: txtPrice,
                money: txtMoney
            }).then(res => {
                console.log(res.data);
                if (!res.data.err) {
                    swal("Thông Báo !", "Cập nhật thành công !", "success");
                }
            });
        } else {

            callApi('createProduct', 'POST', {
                name: txtName,
                price: txtPrice,
                money: txtMoney
            }).then(res => {
                if (res.data.data) {
                    swal("Thông Báo !", "Thêm mới thành công !", "success");


                } else {
                    swal("Thông Báo !", res.data.message, "error");
                }
            });

        }


    }

    componentDidMount() {
        var { match } = this.props;
        if (match) {
            var id = match.params.id;
            callApi('getProductId', 'POST', {
                id: id
            }).then(res => {
                var data = res.data.data;
                this.setState({
                    id: data._id,
                    txtName: data.name,
                    txtMoney: data.money,
                    txtPrice: data.price

                });
            });
        }
    }


    render() {
        var { txtName, txtPrice, txtMoney } = this.state;
        return (


            <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                <form onSubmit={this.onSave}>

                    <legend>Thêm Sản Phẩm</legend>
                    <div className="form-group">
                        <label >Tên Sản Phẩm</label>
                        <input type="text"
                            className="form-control"
                            required
                            name="txtName"
                            value={txtName}
                            onChange={this.onChange}
                        />
                    </div>

                    <div className="form-group">
                        <label>Coin </label>
                        <input type="number"
                            className="form-control"
                            required
                            name="txtPrice"
                            value={txtPrice}
                            onChange={this.onChange}
                        />
                    </div>

                    <div className="form-group">
                        <label >Thành tiền</label>
                        <input type="number"
                            className="form-control"
                            required
                            name="txtMoney"
                            value={txtMoney}
                            onChange={this.onChange}
                        />
                    </div>



                    <button type="submit" className="btn btn-primary">Lưu Lại</button>
                </form>

            </div>


        );
    }


}

export default ProductActionPage;
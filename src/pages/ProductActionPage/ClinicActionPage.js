import React, { Component } from 'react';
import callApi from '../../utils/apiCaller';

class ClinicActionPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            txtID: '',
            txtName: '',
            txtCoin: '',
            txtEmail: '',
            txtWebsite: '',
            txtScale: ''
        }
    }
    componentDidMount() {
        var { match } = this.props;
        var id = match.params.id;
        callApi('getClinicId', 'POST', {
            id: id
        }).then(res => {
            var data = res.data.data;
            if (data) {
                this.setState({
                    txtID: id,
                    txtName: data.name,
                    txtCoin: data.coin,
                    txtEmail: data.idUser.email,
                    txtWebsite: data.website,
                    txtScale: data.scale
                });
            }
        })
    }


    render() {

        var { txtID, txtName, txtCoin, txtEmail, txtWebsite, txtScale } = this.state;
        return (
            <div>
                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                    <form onSubmit={this.onSave}>

                        <legend>Phê Duyệt Bài Việt</legend>
                        <div className="form-group">
                            <label >ID</label>
                            <input type="text"
                                className="form-control"
                                readOnly
                                name="txtId"
                                value={txtID}
                            />
                        </div>
                        <div className="form-group">
                            <label >Tên</label>
                            <input type="text"
                                className="form-control"
                                readOnly
                                name="txtName"
                                value={txtName}
                            />
                        </div>
                        <div className="form-group">
                            <label >Coin</label>
                            <input type="text"
                                className="form-control"
                                readOnly
                                name="txtCoin"
                                value={txtCoin}

                            />
                        </div>
                        <div className="form-group">
                            <label >Email</label>
                            <input type="text"
                                className="form-control"
                                readOnly
                                name="txtEmail"
                                value={txtEmail}
                            />
                        </div>
                        <div className="form-group">
                            <label >Title</label>
                            <input type="text"
                                className="form-control"
                                readOnly
                                name="txtWebsite"
                                value={txtWebsite}
                            />
                        </div>

                        <div className="form-group">
                            <label >Quy Mô</label>
                            <textarea className="form-control" rows="5" name="txtScale" value={txtScale}
                                readOnly></textarea>
                        </div>


                        <p></p>
                    </form>

                </div>

            </div>


        );
    }


}

export default ClinicActionPage;
import React, { Component } from 'react';
import callApi from './../../utils/apiCaller';
import swal from 'sweetalert';
class JobActionPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            txtTitle: '',
            txtSex: '',
            txtChuyenN: '',
            txtKinhN: '',
            txtTypeWork: '',
            txtTerm: '',
            txtRequire: '',
            txtBenefit: '',
            txtStatus: true,
            txtAddress: ''


        }
    }

    componentDidMount() {
        var { match } = this.props;
        if (match) {
            var id = match.params.id;
            callApi('getJobId', 'POST', {
                id: id
            }).then(res => {
                var data = res.data.data;
                if (data) {
                    this.setState({
                        id: id,
                        txtTitle: data.title,
                        txtSex: data.sex,
                        txtChuyenN: data.idSpecialize.name,
                        txtKinhN: data.experience,
                        txtTypeWork: data.typeWork,
                        txtTerm: data.term,
                        txtRequire: data.requirement,
                        txtBenefit: data.benefit,
                        txtAddress: data.address
                    });
                }
            });
        }
    }
    ChangeValue = (e) => {
        if (e.target.value === "true") {
            this.setState({
                txtStatus: true
            });
        } else {
            this.setState({
                txtStatus: false
            });
        }
    }

    onSave = (e) => {
        e.preventDefault();
        var { id, txtStatus } = this.state;
        callApi('updateJob', 'PATCH', {
            id: id,
            status: txtStatus
        }).then(res => {
            swal("Thông Báo !", "Cập nhật trạng thái thành công !", "success");
        });
    }

    render() {

        var { txtTitle, txtSex, txtChuyenN, txtKinhN, txtTypeWork, txtTerm, txtRequire, txtBenefit, txtAddress } = this.state;
        return (
            <div>
                <div className="col-xs-6 col-sm-6 col-md-6 col-lg-6">

                    <form onSubmit={this.onSave}>

                        <legend>Phê Duyệt Bài Việt</legend>
                        <div className="form-group">
                            <label >Title</label>
                            <input type="text"
                                className="form-control"
                                required
                                name="txtTitle"
                                value={txtTitle}
                                readOnly
                            />
                        </div>

                        <div className="form-group">
                            <label >Giới Tính </label>
                            <input type="text"
                                className="form-control"
                                required
                                name="txtSex"
                                value={txtSex}
                                readOnly
                            />
                        </div>

                        <div className="form-group">
                            <label >Chuyên Ngành</label>
                            <input type="text"
                                className="form-control"
                                required
                                name="txtChuyenN"
                                value={txtChuyenN}
                                readOnly
                            />
                        </div>

                        <div className="form-group">
                            <label >Kinh Nghiệm</label>
                            <input type="text"
                                className="form-control"
                                required
                                name="txtKinhN"
                                value={txtKinhN}
                                readOnly
                            />
                        </div>

                        <div className="form-group">
                            <label >Type Work</label>
                            <input type="text"
                                className="form-control"
                                required
                                name="txtTypeWork"
                                value={txtTypeWork}
                                readOnly
                            />
                        </div>

                        <div className="form-group">
                            <label >Term</label>
                            <input type="text"
                                className="form-control"
                                required
                                name="txtTerm"
                                value={txtTerm}
                                readOnly
                            />
                        </div>
                        <div className="form-group">
                            <label >Yêu Cầu Công Việc</label>
                            <textarea className="form-control" rows="5" name="txtRequire" value={txtRequire}
                                readOnly>

                            </textarea>
                        </div>

                        <div className="form-group">
                            <label >Quyền Lợi</label>
                            <textarea className="form-control" rows="5" name="txtBenefit" value={txtBenefit}
                                readOnly></textarea>
                        </div>

                        <div className="form-group">
                            <label >Phê Duyệt </label>
                            <select className="form-control" name="txtStatus" onChange={(e) => this.ChangeValue(e)}>
                                <option value="true">Accept</option>
                                <option value="false">Pending</option>
                            </select>
                        </div>

                        <div className="form-group">
                            <label >Địa Chỉ</label>
                            <textarea className="form-control" rows="5" name="txtAddress" value={txtAddress}
                                readOnly></textarea>
                        </div>

                        <button type="submit" className="btn btn-primary">Confrim</button>
                        <p></p>
                    </form>

                </div>

            </div>


        );
    }


}

export default JobActionPage;
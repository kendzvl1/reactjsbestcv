import React, { Component } from 'react';
import ProductItem from '../../components/ProductItem/ProductItem';
import ProductList from '../../components/ProductList/ProductList';
import callApi from './../../utils/apiCaller';
import swal from 'sweetalert';
class TransactionPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            menus: ['STT', 'ID', 'Tên Gói', 'Coin', 'Thành Tiền', 'Tên Phòng Khám', 'Trạng thái', 'Thời Gian', 'Hành Động'],
            title: 'Danh sách giao dịch',
            txtSearch: '',
            page: 1
        }
    }

    componentDidMount() {
        callApi('getTransaction', 'POST', null).then(res => {
            this.setState({
                products: res.data.data
            });
        });


    }

    onChange = (e) => {
        var data = e.target.value;
        this.setState({
            txtSearch: data
        });
    }

    Onsave = (e) => {
        e.preventDefault();
        var { txtSearch, products } = this.state;
        callApi('getTransactionId', 'POST', {
            id: txtSearch
        }).then(res => {
            if (!res.data.err) {
                this.setState({
                    products: [res.data.data]
                });
            }
            if (res.data.error) {
                var tempProduct = [...products];
                this.setState({
                    products: tempProduct

                })
                swal("Thông Báo !", "Không tồn tại phiên giao dịch", "error");
            }


        });
    }

    funtPage = () => {
        var { page } = this.state;
        page = page - 1;
        callApi('getTransaction', 'POST', {
            page: page
        }).then(res => {
            var data = res.data.data;
            if (data.length > 0) {
                this.setState({
                    page: page,
                    products: data
                })
            }
        });

    }
    funcPage = () => {
        var { page } = this.state;
        page = page + 1;
        callApi('getTransaction', 'POST', {
            page: page
        }).then(res => {
            var data = res.data.data;
            if (data.length > 0) {
                this.setState({
                    page: page,
                    products: data
                })
            }

        });



    }
    CoinPlus = (product) => {

        var data = product;
        var { products } = this.state;
        var rs = JSON.parse(localStorage.getItem('user'));
        var idAdmin = rs.id;
        var index = this.findIndex(products, data._id);

        if (index !== -1) {
            products[index].state = "true";
            this.setState({
                products: products
            })
            this.upDateCoin(data.idClinic._id, data.idProduct.price);
            callApi('updateTransaction', 'PATCH', {
                id: data._id,
                state: true,
                createBy: idAdmin
            }).then(res => {
                console.log(res.data);
                if (!res.data.error) {
                    swal("Thông Báo !", "Nạp tiền thành công !", "success");
                }

            });
            callApi('sendMail', 'POST', {
                email: data.idClinic.idUser.email,
                coin: data.idProduct.price
            }).then(res => {
                console.log(res.data);
            });
        }

    }

    upDateCoin = (id, coin) => {
        callApi('updateCoin', 'PATCH', {
            id: id,
            coin: coin
        }).then(res => {

        });
    }
    findIndex = (products, id) => {
        var result = -1;
        products.forEach((product, index) => {
            if (product._id === id) {
                result = index;
            }
        });
        return result
    }


    render() {

        const { menus, title, products, txtSearch } = this.state;
        return (

            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <div className="row">
                    <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <form onSubmit={this.Onsave}>
                            <legend>Tìm Kiếm</legend>

                            <div className="form-group">
                                <input type="text" className="form-control" value={txtSearch} onChange={this.onChange} required />
                            </div>



                            <button type="submit" className="btn btn-primary " >Search</button>
                        </form>
                    </div>

                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-8">

                    </div>

                </div>

                <p></p>
                <ProductList arrs={menus} title={title}>
                    {this.showProducts(products)}
                </ProductList>

                <div className="text-center">
                    <ul className="pagination">
                        <li style={{ cursor: "pointer" }} className="page-item "><span className="page-link" onClick={this.funtPage}>Previous</span></li>
                        <li className="page-item"><span className="page-link" >1</span></li>
                        <li className="page-item"><span className="page-link" >2</span></li>
                        <li className="page-item"><span className="page-link" >3</span></li>

                        <li style={{ cursor: "pointer" }} className="page-item "><span className="page-link" onClick={this.funcPage}>Next</span></li>
                    </ul>
                </div>


            </div>


        );

    }

    showProducts(products) {
        var result = null;
        if (products.length > 0) {
            result = products.map((product, index) => {
                return (<ProductItem
                    key={index}
                    product={product}
                    index={index}
                    onDelete={this.onDelete}
                    CoinPlus={this.CoinPlus}
                    conDition="transaction"
                />);
            })
        }
        return result;
    }


}

export default TransactionPage;
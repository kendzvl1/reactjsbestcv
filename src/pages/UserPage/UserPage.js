import React, { Component } from 'react';
import ProductItem from '../../components/ProductItem/ProductItem';
import ProductList from '../../components/ProductList/ProductList';
import callApi from '../../utils/apiCaller';
class UserPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            menus: ['STT', 'ID', 'Tên', 'Email', 'Coin', 'Hành Động'],
            title: 'Danh sách tài khoản',
            filterName: '',
            sortCoin: 0

        }
    }





    onChangeValue = (e) => {
        var mode = e.target.value;
        var url = '';

        if (mode === "1") {
            url = 'getDoctor';
        } else {
            url = 'getClinic';
        }
        callApi(url, 'GET', null).then(res => {
            this.setState({
                products: res.data.data
            });

        })

    }



    componentDidMount() {

        callApi('getDoctor', 'GET', null).then(res => {

            this.setState({
                products: res.data.data
            });
        });

    }

    onFilterName = (e) => {
        this.setState({
            filterName: e.target.value.toLowerCase()
        })
    }
    sortByCoin = () => {
        var { products } = this.state;
        var result = this.bubbleSort(products);
        this.setState({
            products: result
        });
    }

    bubbleSort = (arrs) => {
        if (arrs) {
            for (var i = 0; i < arrs.length; i++) {
                var max = arrs[i].coin;
                var obJect = arrs[i];
                for (var j = i + 1; j < arrs.length; j++) {
                    if (max < arrs[j].coin) {
                        var temp = arrs[i];
                        max = arrs[j].coin;
                        obJect = arrs[j];
                        arrs[j] = temp;
                    }
                    arrs[i] = obJect;
                }
            }
            return arrs;
        }
    }


    render() {

        var { menus, title, products, filterName } = this.state;
        if (filterName) {
            products = products.filter((product) => {
                var name = "";
                if (product.firstName) {
                    name = product.firstName + " " + product.lastName;
                } else {
                    name = product.name;
                }
                return name.toLowerCase().indexOf(filterName) !== -1;
            })
        }
        return (

            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">


                <div className="row">
                    <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">

                        <legend>Lọc Theo :</legend>

                        <div className="form-group">

                            <select name="" id="input" className="form-control" onChange={(e) => this.onChangeValue(e)} >
                                <option value="1">Bác Sĩ</option>
                                <option value="2">Phòng Khám</option>
                            </select>

                        </div>



                        <p></p>
                    </div>
                    <legend>.</legend>
                    <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div className="form-group">
                            <input type="text" name="filterName" className="form-control" value={filterName} onChange={(e) => this.onFilterName(e)} placeholder="Nguyễn Văn A" />

                        </div>
                    </div>

                    <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <button type="button" className="btn btn-large btn-block btn-info" onClick={this.sortByCoin}>Sort By Coin</button>


                    </div>

                </div>


                <ProductList arrs={menus} title={title}>
                    {this.showProducts(products)}
                </ProductList>

            </div>


        );

    }

    showProducts(products) {
        var result = null;
        if (products.length > 0) {
            result = products.map((product, index) => {
                return (<ProductItem
                    key={index}
                    product={product}
                    index={index}
                    conDition="user"
                />);
            })
        }
        return result;
    }


}

export default UserPage;
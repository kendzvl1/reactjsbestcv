import React, { Component } from 'react';
import ProductList from './../../components/ProductList/ProductList';
import ProductItem from './../../components/ProductItem/ProductItem';
import callApi from './../../utils/apiCaller';
import { Link } from 'react-router-dom';
class HomePage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            products: [],
            menus: ['STT', 'Title', 'Giới Tính', 'Kinh Nghiệm', 'Tên Phòng Khám', 'Trạng Thái', 'Hành Động'],
            title: 'Danh Sách Bài Viết'
        };
    }

    componentDidMount() {
        callApi('getJob', 'GET', null).then(res => {
            console.log(res.data.data);
            this.setState({
                products: res.data.data
            });
        });

    }




    render() {
        var { products, menus, title } = this.state;
        return (
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <Link to="/" className="btn btn-info mb-10">Danh Sách Công Việc</Link>
                <ProductList arrs={menus} title={title}>
                    {this.showProducts(products)}
                </ProductList>
            </div>



        );
    }

    showProducts(products) {
        var result = null;
        if (products.length > 0) {
            result = products.map((product, index) => {
                return (<ProductItem
                    key={index}
                    product={product}
                    index={index}
                    conDition="job"
                />);
            })
        }
        return result;
    }

}

export default HomePage;
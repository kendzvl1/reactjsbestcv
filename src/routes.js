import React from 'react';
import HomePage from './pages/HomePage/HomePage';
import NotFoundPage from './pages/NotFoundPage/NotFoundPage';
import ClinicActionPage from './pages/ProductActionPage/ClinicActionPage';
import JobActionPage from './pages/ProductActionPage/JobActionPage';
import ProductActionPage from './pages/ProductActionPage/ProductActionPage';
import ProductListPage from './pages/ProductListPage/ProductListPage';
import TransactionPage from './pages/TransactionPage/TransactionPage';
import UserPage from './pages/UserPage/UserPage';

const routes = [
    {
        path: '/',
        exact: true,
        main: () => <HomePage />
    },
    {
        path: '/product-list',
        exact: false,
        main: () => <ProductListPage />
    },
    {
        path: '/product/add',
        exact: false,
        main: () => <ProductActionPage />
    },
    {
        path: '/product/:id/edit',
        exact: false,
        main: ({ match }) => <ProductActionPage match={match} />
    },
    {
        path: '/job/:id/edit',
        exact: false,
        main: ({ match }) => <JobActionPage match={match} />
    },
    {
        path: '/usermanage',
        exact: false,
        main: () => <UserPage />
    },
    {
        path: '/user/:id',
        exact: false,
        main: ({ match }) => <ClinicActionPage match={match} />
    },
    {
        path: '/transaction',
        exact: false,
        main: () => <TransactionPage />
    },
    {
        path: '',
        exact: false,
        main: () => <NotFoundPage />
    }

];

export default routes;
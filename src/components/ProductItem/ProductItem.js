import React, { Component } from 'react';
import swal from 'sweetalert';
import { Link } from 'react-router-dom';
import moment from 'moment';
import callApi from '../../utils/apiCaller';
class ProductItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: []
        };
    }
    onDelete = (id) => {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover !",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    this.props.onDelete(id);
                    swal(`${id} Đã được xóa !`, {
                        icon: "success",
                    });
                } else {

                }
            });
    }
    CoinPlus = (product) => {
        this.props.CoinPlus(product);
    }

    DeleteUser = (id, isDoctor) => {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover !",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    var url = 'deleteDoctor';
                    if (isDoctor !== true) {
                        url = 'deleteClinic';
                    }
                    callApi(url, 'DELETE', {
                        id: id
                    });

                    swal(" Xóa thành công !", {
                        icon: "success",
                    });
                }
            });
    }




    render() {
        var { product, index, conDition } = this.props;

        return (

            this.preRender(product, index, conDition)


        );

    }
    preRender = (product, index, conDition) => {
        var result = null;
        if (conDition === "product") {
            var xMoney = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(product.money);
            result =
                <tr>
                    <td>{index + 1}</td>
                    <td>{product._id}</td>
                    <td>{product.name}</td>
                    <td>{product.price}</td>
                    <td>{xMoney}</td>

                    <td>
                        <Link to={`/product/${product._id}/edit`} className="btn btn-success mr-10" >Sửa</Link>

                        <button type="button" className="btn btn-danger" onClick={() => this.onDelete(product._id)}>Xóa</button>

                    </td>
                </tr>
        } else if (conDition === "transaction") {
            var statusName = null;
            var statusClass = null;
            var flag = null;
            if (product.idProduct) {
                var xMoneys = new Intl.NumberFormat('vi-VN', { style: 'currency', currency: 'VND' }).format(product.idProduct.money);
                if (product.state === 'true') {
                    flag = 'none';
                    statusName = 'Accepted';
                    statusClass = 'success';
                } else {
                    flag = 'block';
                    statusName = 'Pending';
                    statusClass = 'warning';
                }

                result = <tr>
                    <td>{index + 1}</td>
                    <td>{product._id}</td>
                    <td>{product.idProduct.name}</td>
                    <td>{product.idProduct.price}</td>
                    <td>{xMoneys}</td>
                    <td>{product.idClinic.name}</td>
                    <td>
                        <span className={`label label-${statusClass}`}>{statusName}</span>
                    </td>
                    <td>{moment(product.created).locale('vi').format('lll')}</td>
                    <td style={{ display: flag }}>
                        <button onClick={() => this.CoinPlus(product)} className="btn btn-success mr-10" >Confirm</button>
                    </td>

                </tr>

            }



        } else if (conDition === "job") {
            var statusNames = null;
            var statusClasss = null;
            if (product.status === true) {
                statusNames = 'Accepted';
                statusClasss = 'success';
            } else {
                statusNames = 'Pending';
                statusClasss = 'warning';
            }
            if (product.idClinic) {
                result = <tr>
                    <td>{index + 1}</td>
                    <td>{product.title}</td>
                    <td>{product.sex}</td>
                    <td>{product.experience}</td>
                    <td>{product.idClinic.name}</td>
                    <td>
                        <span className={`label label-${statusClasss}`}>{statusNames}</span>
                    </td>

                    <td >
                        <Link to={`/job/${product._id}/edit`} className="btn btn-default mr-10" >Chi Tiết</Link>
                    </td>

                </tr>
            }

        }
        else if (conDition === "user") {

            if (product.idUser) {
                var xName = '';
                var xCoin = 0;
                var xId = product._id;
                var isDoctor = false;
                if (product.firstName) {
                    isDoctor = true;
                    xName = product.firstName + ' ' + product.lastName;
                } else {
                    xName = product.name;
                    xCoin = product.coin;
                }
                result = <tr>
                    <td>{index + 1}</td>
                    <td>{product._id}</td>
                    <td>{xName}</td>
                    <td>{product.idUser.email}</td>
                    <td>{new Intl.NumberFormat().format(xCoin)}</td>



                    <td >
                        <Link to={`/user/${product._id}`} className="btn btn-default mr-10" >Chi Tiết</Link>

                        <button type="button" className="btn btn-danger " onClick={() => this.DeleteUser(xId, isDoctor)} >Xóa</button>
                    </td>

                </tr>
            }

        }


        return result;
    }


}

export default ProductItem;
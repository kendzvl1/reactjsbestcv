import React, { Component } from 'react';

class ProductList extends Component {

    render() {
        var { arrs, title } = this.props;
        return (

            <div className="panel panel-primary">
                <div className="panel-heading">
                    <h3 className="panel-title">{title}</h3>
                </div>
                <div className="panel-body">

                    <table className="table table-bordered table-hover">
                        <thead>
                            <tr>
                                {this.showField(arrs)}
                            </tr>
                        </thead>
                        <tbody>
                            {this.props.children}
                        </tbody>
                    </table>

                </div>
            </div>




        );
    }

    showField = (arrs) => {
        var result = null;
        if (arrs.length > 0) {
            result = arrs.map((arr, index) => {
                return (
                    <th key={index}>{arr}</th>
                );
            });
        }
        return result;
    }

}

export default ProductList;